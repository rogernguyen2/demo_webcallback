# demo_web callback



web callback demo using studio API for onDemand campaign. 

i suggest you replace the logo and wording to tailor it to your demo audience (if you can, otherwise ask me for help).

this is really hack-y code done across a day. i know there's some bad practices implemented like not following DRY, no/poor error handling, etc. but it works (solid as a straw). at some point i plan to clean it up and improve on it.

the back-end is nodeJS (using expressJS as the framework), front-end is just HTML/CSS/jQuery static page.

## applications:


- SMS other content based on studio task after external event 'x'
	- receipt after web check out from online store
	- shipping details for the same thing
- call a number and execute a task after some external event 
	- get an agent to call back a lead for a product 

## Behaviour: 

what this thing does is fires an onDemand API request for an onDemand Action (based on your chosen studio region: US or AUS and your On Demand API Key) after you've opened up a form element and filled in some details, based on the target number you input. it uses a script created inside studio for the callback task.

The script basically consumes the additional parameters posted by the onDemand API and then fills them into variables and the detail is read back.

if you wish to re-purpose it for your own demo then you will need to change the HTML template (if you can otherwise i can help). 

currently this is hosted on heroku @ https://rugged-sequoia-78352.herokuapp.com/

(heroku free accounts put their app servers to sleep after no activity for 30 mins, a workaround is to put a setInterval function to self-ping after 30 mins or use http://kaffeine.herokuapp.com/)

