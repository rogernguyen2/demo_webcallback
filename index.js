// Imports the Google Cloud client library
var express = require('express');
http = require('http');
var app = express();
var server = http.createServer(app);
var request = require('request');
var url = require('url');
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(express.static('public'));
app.use('/public', express.static(__dirname + '/public'));

//demand URL for onDemand Campaign API
const ausdemandURL = 'https://ausstudio.inferencecommunications.com/studio_instance/api/v1/outbound/call/' 
const ausvariableURL = 'https://ausstudio.inferencecommunications.com/studio_instance/studio-api/v1/variable/update-value/'
const usdemandURL = 'https://usstudio.inferencecommunications.com/studio_instance/api/v1/outbound/call/'
const usvariableURL = 'https://usstudio.inferencecommunications.com/studio_instance/studio-api/v1/variable/update-value/'
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
    //res.send(messages);
    //res.sendFile(__dirname + '/client/client.js');
});

app.post('/call', function(req,res){

  var name = req.body.contactName;
  var contactNo = req.body.contactNumber;
  var token = req.body.token;
  var region = req.body.region;
  var targetNo = req.body.targetNo;
  var campaignAPIKey = req.body.campaignAPIKey;
  var pageName = req.body.pageName;
  var productName = req.body.productName;



  if (region == 'us') {

    demandURL = usdemandURL + campaignAPIKey + '/' + targetNo


  } else {
    demandURL = ausdemandURL + campaignAPIKey + '/' + targetNo

  }

   var onDemand_options = {
      url: demandURL, // demandURL,
      method: 'POST',
      form:{
        name: name,
        contactNo: contactNo,
        pageName: pageName,
        productName: productName
      }
  };


    request(onDemand_options, function (error, response, body) { 
    if (!error && response.statusCode === 200) { 
      console.log('POST great success');
      console.log(onDemand_options);
      console.log(body); 
      res.send(body); 
    }
    console.log('POST error');
    console.log(body);
    console.log(onDemand_options); 
   }); 


});


var port = process.env.PORT || (process.argv[2] || 8010);
port = (typeof port === "number") ? port : 8010;

setInterval(function() {
    console.log("app pinged");
    http.get("http://rugged-sequoia-78352.herokuapp.com");
}, 300000);

if(!module.parent){ server.listen(process.env.PORT || 8010); }

console.log("Application started. Listening on port:" + port);

  module.exports = app;